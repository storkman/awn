#define _DEFAULT_SOURCE
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <poll.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <utf.h>

#include "def.h"
#include "ansi.h"
#include "text.h"
#include "parse.h"
#include "file.h"
#include "term.h"
#include "proc.h"

struct Proc *procs;

void
proc_add_arg(Proc *p, const char *s)
{
	if (p->argc >= SIZE(p->argv)) {
		fprintf(stderr, "Too many arguments to %s.\n", p->argv[0]);
		p->error = ENOMEM;
		return;
	}
	p->argv[p->argc] = strdup(s);
	p->argc++;
}

void
proc_attach_term(Proc *p, struct File *f)
{
	proc_set_file(p, f);
	term_set_interactive(&p->term, 1);
}

void
proc_detach_file(Proc *p, struct File *f)
{
	if (p->term.file.file == f)
		file_ref_detach(&p->term.file);
	if (p->term.out_file.file == f)
		file_ref_detach(&p->term.out_file);
}

void
proc_free(Proc *p)
{
	char **s;
	struct File *ref_file = p->term.file.file;
	term_deinit(&p->term);
	if (ref_file && ref_file->client == p)
		ref_file->client = NULL;
	for (s = p->env; *s; s++)
		free(*s);
	for (s = p->argv; *s; s++)
		free(*s);
	free(p);
}

void
proc_handleall(struct pollfd *fds, int n)
{
	struct Proc *p;
	for (p = procs; p; p = p->next) {
		if (n < 2)
			return;
		term_handle_fd(&p->term, fds);
		n -= 2;
		fds += 2;
	}
}

void
proc_insert_at(Proc *p, struct File *f, int64 p0, int64 p1)
{
	file_ref(&p->term.out_file, f, p0, p1);
}

struct Proc*
proc_new(const char *s)
{
	struct Proc *p = calloc(1, sizeof(*p));
	if (!p)
		die("out of memory\n");
	term_init(&p->term);
	p->argv[0] = strdup(s);
	p->argc = 1;
	return p;
}

int
proc_pollfds(struct pollfd *fds, int nfds)
{
	int n = 0;
	struct Proc *p, *dead;
	struct Proc **live = &procs;
	p = procs;
	while (p) {
		if (term_is_attached(&p->term)) {
			*live = p;
			live = &p->next;
			p = p->next;
			continue;
		}
		dead = p;
		p = p->next;
		proc_free(dead);
	}
	*live = NULL;
	
	for (p = procs; p; p = p->next) {
		if (n + 2 > nfds)
			return n;
		term_poll_fd(&p->term, fds);
		n += 2;
		fds += 2;
	}
	return n;
}

void
proc_send(struct Proc *p, const char *b, int64 n)
{
	return term_send(&p->term, b, n);
}

void
proc_setenv(struct Proc *p, const char *name, const char *val)
{
	int len = strlen(name) + 1 + strlen(val);
	char *env = malloc(len+1);
	int i;
	if (!env)
		die("out of memory\n");
	snprintf(env, len+1, "%s=%s", name, val);
	for (i = 0; i < SIZE(p->env)-1; i++) {
		if (p->env[i])
			continue;
		p->env[i] = env;
		return;
	}
	fprintf(stderr, "Too many environment variables. $%s not set.\n", name);
}

void
proc_set_file(struct Proc *p, struct File *f)
{
	char name[PATHLEN];
	str_copy(name, f->name, sizeof(name));
	proc_setwd(p, dirname(name));
	proc_setenv(p, "file", f->name);
	proc_setenv(p, "%", f->name);
	file_ref(&p->term.file, f, -1, -1);
}

void
proc_setwd(struct Proc *p, const char *dir)
{
	if (strlen(dir) + 1 > sizeof(p->term.wdir)) {
		fprintf(stderr, "can't set working directory for process: path too long\n");
		return;
	}
	str_copy(p->term.wdir, dir, sizeof(p->term.wdir));
}

static int
fork_redir(int *input, int *output)
{
	int infd[2], outfd[2];
	int err, pid;
	if (pipe(outfd)) {
		err = errno;
		perror("failed to create pipe");
		return -err;
	}
	if (pipe(infd)) {
		err = errno;
		perror("failed to create pipe");
		close(outfd[0]);
		close(outfd[1]);
		return -err;
	}
	pid = fork();
	if (pid == -1) {
		err = errno;
		perror("failed to fork");
		close(outfd[0]);
		close(outfd[1]);
		close(infd[0]);
		close(infd[1]);
		return -err;
	}
	if (pid == 0) {
		close(infd[1]);
		close(outfd[0]);
		if (input)
			*input = infd[0];
		else
			close(infd[0]);
		if (output)
			*output = outfd[1];
		else
			close(outfd[1]);
		return 0;
	}
	close(infd[0]);
	close(outfd[1]);
	if (input)
		*input = infd[1];
	else
		close(infd[1]);
	if (output)
		*output = outfd[0];
	else
		close(outfd[0]);
	return pid;
}

static int
runcommand(struct Proc *cmd)
{
	int inf, outf, pid;
	pid = fork_redir(&inf, &outf);
	if (pid < 0) {
		cmd->error = -pid;
		printf("failed to start command: %s", strerror(-pid));
		return -1;
	}
	if (pid == 0) {
		int ignore = 0;
		char **var;
		close_fds();
		dup2(inf, 0);
		dup2(outf, 1);
		dup2(outf, 2);
		for (var = cmd->env; *var; var++)
			putenv(*var);
		if (cmd->term.wdir[0])
			ignore = chdir(cmd->term.wdir);	/* wdir might not exist if started from a virtual file */
		(void) ignore;
		execvp(cmd->argv[0], cmd->argv);
		die("exec failed: %s:", cmd->argv[0]);
	}
	cmd->term.pid = pid;
	cmd->term.infd = inf;
	return outf;
}

static void
runtranslate(struct Proc *cmd, int cmdout)
{
	int pid;
	int outf;
	pid = fork_redir(NULL, &outf);
	if (pid < 0) {
		printf("failed to start command output sanitizer: %s", strerror(-pid));
		return;
	}
	if (pid == 0) {
		close_fds();
		exit(ansi_run(outf, cmdout));
	}
	cmd->term.sanitizer_pid = pid;
	cmd->term.outfd = outf;
}

void
proc_start(struct Proc *cmd)
{
	int outfd;
	cmd->next = procs;
	procs = cmd;
	outfd = runcommand(cmd);
	if (outfd < 0)
		return;
	runtranslate(cmd, outfd);
	close(outfd);
}
