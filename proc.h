struct Proc {
	int error;
	
	Term term;
	
	char	*env[16];	/* vector of strings to be passed to setenv(3) */
	char	*argv[16];
	int	argc;
	
	Proc *next;
};

extern Proc *procs;

void	proc_add_arg(Proc*, const char*);
void	proc_attach_term(Proc*, struct File*);
void	proc_detach_file(Proc*, struct File*);
void	proc_free(Proc*);
void	proc_handleall(struct pollfd*, int n);
void	proc_insert_at(Proc*, struct File*, int64, int64);
Proc*	proc_new(const char*);
int	proc_pollfds(struct pollfd*, int nfds);
void	proc_send(Proc*, const char*, int64);
void	proc_setenv(Proc*, const char*, const char*);
void	proc_set_file(Proc*, struct File*);
void	proc_setwd(Proc*, const char*);
void	proc_start(Proc*);
