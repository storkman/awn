#define _DEFAULT_SOURCE
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <poll.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <utf.h>

#include "def.h"
#include "ansi.h"
#include "text.h"
#include "lex.h"
#include "file.h"
#include "window.h"
#include "term.h"

#define MAX_CMD_LEN 512

static const char term_command_start = 0x10;
static const char term_command_end = 0x02;

static void term_append(Term*, const char*, int);
static void term_append_command(Term*, const char*, int);
static void term_command_error(Term*);
static void term_end_command(Term*);
static void term_prog_output(Term*, const char*, int);
static void term_run_command(Term*, const char*, int);

void
term_deinit(Term *t)
{
	if (t->infd)
		close(t->infd);
	if (t->outfd)
		close(t->outfd);
	if (t->input)
		(void) text_free(t->input);
}

void
term_init(Term *t)
{
	memset(t, 0, sizeof(*t));
	t->input = text_new();
	t->command = text_new();
}

int
term_is_attached(Term *t)
{
	int status;
	if (t->pid > 0 && waitpid(t->pid, &status, WNOHANG) > 0) {
		t->pid = -1;
		if (t->infd >= 0)
			close(t->infd);
		t->infd = -1;
	}
	if (t->sanitizer_pid > 0 && waitpid(t->sanitizer_pid, &status, WNOHANG) > 0)
		t->sanitizer_pid = -1;
	if (t->infd > 0 || t->outfd > 0 || t->pid > 0 || t->sanitizer_pid > 0)
		return 1;
	else
		return 0;
}

void
term_handle_fd(Term *t, struct pollfd fd[2])
{
	int64 len = 0;
	if (t->infd > 0)
		fcntl(t->infd, F_SETFL, O_NONBLOCK | O_APPEND);
	if (t->outfd > 0)
		fcntl(t->outfd, F_SETFL, O_NONBLOCK);
	if (fd[0].revents & POLLERR) {
		if (t->infd > 0)
			close(t->infd);
		t->infd = -1;
		if (t->input)
			t->input = text_free(t->input);
	} else if (t->infd > 0 && (fd[0].revents & (POLLOUT|POLLERR))) {
		len = text_len(t->input);
		if (len > BLOCKSIZE)
			len = BLOCKSIZE;
		if (len == 0 && !t->interactive) {
			close(t->infd);
			t->infd = -1;
			t->input = text_free(t->input);
		}
	}
	if (len > 0) {
		char *s = text_ref(t->input, 0, len);
		len = write(t->infd, s, len);
		if (len > 0) {
			text_change(t->input, 0, len, "", 0);
		} else if (errno != EAGAIN) {
			t->error = errno;
			close(t->infd);
			t->infd = -1;
			t->input = text_free(t->input);
		}
	}
	
	if (t->outfd > 0 && (fd[1].revents & (POLLIN|POLLHUP|POLLERR))) {
		char buf[BLOCKSIZE];
		int n = read(t->outfd, buf, sizeof(buf));
		if (n > 0) {
			term_prog_output(t, buf, n);
		} else if (n == 0) {
			close(t->outfd);
			t->outfd = -1;
		} else if (errno != EAGAIN) {
			t->error = errno;
			perror("command read error");
			close(t->outfd);
			t->outfd = -1;
		}
	}
	if (t->infd > 0)
		fcntl(t->infd, F_SETFL, O_APPEND);
	if (t->outfd > 0)
		fcntl(t->outfd, F_SETFL, 0);
}

void
term_hang_up(Term *t)
{
	t->eof = 1;
}

static void
term_prog_output(Term *t, const char *b, int len)
{
	const char *here;
	const char *end = b + len;
	const char *next = end;
	char look_for = 0, cmd;
	
	for (here = b; here < end; here = next) {
		cmd = t->reading_command;
		look_for = cmd ? term_command_end : term_command_start;
		if (*here == look_for) {
			if (cmd)
				term_end_command(t);
			else
				t->reading_command = 1;
			next = here + 1;
			continue;
		}
		
		next = memchr(here, look_for, end - here);
		if (!next)
			next = end;
		
		if (cmd)
			term_append_command(t, here, next - here);
		else
			term_append(t, here, next - here);
	}
}

void
term_append(Term *t, const char *b, int len)
{
	struct FileRef *f = &t->out_file;
	if (f->discard)
		return;
	if (!f->file)
		return (void) errorwin_append(t->wdir, b, len);
	file_ref_update(f);
	file_ref_insert(f, b, len);
}

void
term_append_command(Term *t, const char *b, int len)
{
	int cl = text_len(t->command);
	if (cl + len > MAX_CMD_LEN) {
		int discard = MAX_CMD_LEN - cl;
		term_append(t, b + discard, len - discard);
		t->reading_command = 0;
		return;
	}
	text_append(t->command, b, len);
}

void
term_command_error(Term *t)
{
}

void
term_end_command(Term *t)
{
	t->reading_command = 0;
	term_run_command(t, text_all(t->command), text_len(t->command));
	text_change(t->command, 0, text_len(t->command), "", 0);
}

static void
_tcmd_clear(Term *t)
{
	struct FileRef *fr = &t->out_file;
	if (!fr->file || fr->discard)
		return;
	file_change(fr->file, 0, text_len(fr->file->txt), "", 0, SOURCE_CMD);
	if (fr->p0 < 0)
		file_ref(fr, fr->file, -1, -1);
	else
		file_ref(fr, fr->file, 0, 0);
}

static void
_tcmd_popup(Term *t)
{
	struct Win *m, *w;
	const char *name;
	m = win_find_file(t->file.file);
	name = m ? m->file->name : "";
	w = win_popup_new(m, name, "", 0);
	win_popup_init(w);
	file_ref(&t->out_file, w->file, 0, 0);
}

static void
_tcmd_resize(Term *t)
{
	win_shrink(win_find_file(t->file.file));
}

struct TermCmd {
	void (*fn)(Term*);
	int has_args;
};

static void
term_run_command(Term *t, const char *s, int len)
{
	static const struct TermCmd cmds[] = {
	['C']	=	{_tcmd_clear,	0},
	['P']	=	{_tcmd_popup,	0},
	['R']	=	{_tcmd_resize,	0},
	};
	
	Lex lex;
	struct TermCmd cmd;
	unsigned char cmdb = 0;
	
	lex_init(&lex, s, len);
	cmdb = lex_getc(&lex);
	if (cmdb < 0 || cmdb >= SIZE(cmds))
		return term_command_error(t);
	cmd = cmds[cmdb];
	if (!cmd.fn)
		return term_command_error(t);
	
	if (!cmd.has_args && !lex_eof(&lex))
		return term_command_error(t);
	cmd.fn(t);
}

static void
_pollfd_in(struct pollfd *fd, Term *t)
{
	if (t->infd < 0) {
		t->input = text_free(t->input);
		fd->fd = -1;
		return;
	}
	if (!t->input || text_len(t->input) == 0) {
		if (t->eof) {
			close(t->infd);
			t->input = text_free(t->input);
		}
		fd->fd = -1;
		return;
	}
	fd->fd = t->infd;
	fd->events = POLLOUT;
}

static void
_pollfd_out(struct pollfd *fd, Term *t)
{
	if (t->outfd >= 0) {
		fd->fd = t->outfd;
		fd->events = POLLIN;
	} else
		fd->fd = -1;
}

void
term_poll_fd(Term *t, struct pollfd fds[2])
{
	memset(fds, 0, sizeof(fds[0])*2);
	_pollfd_in(&fds[0], t);
	_pollfd_out(&fds[1], t);
}	

void
term_send(Term *t, const char *b, int64 n)
{
	if (t->eof)
		return;
	if (!t->input)
		t->input = text_new();
	text_append(t->input, b, n);
}

void
term_set_interactive(Term *t, int en)
{
	t->interactive = en;
}
