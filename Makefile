CC = cc
CFLAGS += --std=c99 -Werror=implicit -Wuninitialized -Wunused

OBJ = limed.o ansi.o text.o typo.o file.o layout.o lex.o parse.o panel.o proc.o term.o
.PHONY: all clean

all: limed ansi2loz

tags: *.c *.h
	ctags -n *.c *.h

config.h:
	cp config.def.h $@

limed: ${OBJ}
	${CC} -o $@ $^ -lm -ldl -L/usr/X11R6/lib -lX11 -lXext -lXft \
		$$(pkg-config --libs fontconfig) -lutf

limed.o: limed.c *.h config.h
	${CC} ${CFLAGS} -DVERSION=\""$$(git describe --always) - $$(date -R)"\" \
		$$(pkg-config --cflags fontconfig) -c $<

panel.o: panel.c *.h
	${CC} ${CFLAGS} $$(pkg-config --cflags fontconfig) -c $<

typo.o: typo.c *.h
	${CC} ${CFLAGS} $$(pkg-config --cflags fontconfig) -c $<

ansi2loz: ansi2loz.o ansi.o lex.o
	${CC} -o $@ $^

lime-tags: lime-tags.c
	${CC} -o $@ $^ -lreadtags

%.o: %.c *.h
	${CC} ${CFLAGS} -c $<

clean:
	rm -f limed lime-tags ansi2loz *.o

