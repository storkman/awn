#define _DEFAULT_SOURCE
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <poll.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <utf.h>

#include "def.h"
#include "ansi.h"
#include "text.h"
#include "parse.h"
#include "file.h"
#include "term.h"
#include "proc.h"

enum {CT_NOP, CT_INS, CT_DEL, CT_REP};

static int
_change_type(struct Change *c)
{
	if (c->p0 == c->p1) {
		if (c->len == 0)
			return CT_NOP;
		return CT_INS;
	}
	if (c->len == 0)
		return CT_DEL;
	return CT_REP;
}

static char*
_change_append(char *a, int64 an, char *b, int64 bn)
{
	a = realloc(a, an + bn);
	if (!a)
		die("out of memory\n");
	memmove(a + an, b, bn);
	return a;
}

static char*
_change_concat(char *a, int64 an, char *b, int64 bn)
{
	char *s = malloc(an + bn);
	if (!s)
		die("out of memory\n");
	memmove(s, a, an);
	memmove(s+an, b, bn);
	return s;
}

int
change_merge(struct Change *a, struct Change *b)
{
	int at, bt;
	int64 alen, blen;
	if (a->later || a->final)
		return 0;
	if (a->source != SOURCE_TYPED || b->source != SOURCE_TYPED)
		return 0;
	at = _change_type(a);
	bt = _change_type(b);
	if (at == CT_NOP) {
		b->prev = a->prev;
		b->earlier = a->earlier;
		*a = *b;
		return 1;
	}
	if (bt == CT_REP)
		return 0;
	if (bt == CT_NOP)
		return 1;
	if (bt == CT_INS) {
		if (at != CT_INS && at != CT_REP)
			return 0;
		if (a->p0 + a->len != b->p0)
			return 0;
		a->to = _change_append(a->to, a->len, b->to, b->len);
		a->len = a->len + b->len;
		return 1;
	}
	if (at != CT_DEL)
		return 0;
	
	if (a->p0 != b->p1)
		return 0;
	{
		char *nc;
		alen = a->p1 - a->p0;
		blen = b->p1 - b->p0;
		nc = _change_concat(b->from, blen, a->from, alen);
		free(a->from);
		a->from = nc;
		a->p0 = b->p0;
	}
	return 1;
}

void
change_rev(struct Change *ch)
{
	struct Change c = *ch;
	ch->p0 = c.p0;
	ch->p1 = c.p0 + c.len;
	ch->to = c.from;
	ch->from = c.to;
	ch->len = c.p1 - c.p0;
}

struct Change*
change_add(struct History *h, struct Change *c)
{
	struct Change *nc;
	if (h->latest && h->latest == h->last && change_merge(h->last, c))
		return h->last;
	nc = malloc(sizeof(*nc));
	if (!nc)
		die("out of memory\n");
	*nc = *c;
	nc->earlier = h->latest;
	if (h->latest)
		h->latest->later = nc;
	h->latest = nc;
	nc->prev = h->last;
	if (h->last)
		h->last->next = nc;
	else
		h->first = nc;
	h->last = nc;
	return nc;
}

void
change_free(struct Change *c)
{
	if (c->from)
		free(c->from);
	if (c->to)
		free(c->to);
	free(c);
}

struct Change*
file_add_change(struct File *f, struct Change *c)
{
	text_change(f->txt, c->p0, c->p1, c->to, c->len);
	return change_add(&f->history, c);
}

void
file_autosave(struct File *f)
{
	struct timespec now;
	time_t last;
	char name[PATHLEN];
	size_t n;
	
	if (clock_gettime(CLOCK_MONOTONIC, &now)
	&& clock_gettime(CLOCK_REALTIME, &now)) {
		fprintf(stderr, "Can't read the clock! Won't autosave!\n");
		return;
	}
	last = f->autosave_last;
	if (last == 0) {
		f->autosave_last = now.tv_sec;
		f->autosave_chg = f->history.last;
		return;
	}
	if (f->autosave_chg == f->history.last) {
		f->autosave_last = now.tv_sec;
		return;
	}
	if (now.tv_sec - last < AUTOSAVE_INTERVAL)
		return;
	
	n = snprintf(name, sizeof(name), "%s~", f->name);
	if (n >= sizeof(name)) {
		fprintf(stderr, "Can't autosave: file path too long\n");
		f->autosave_last = now.tv_sec;
		return;
	}
	if (file_put(f, name)) {
		perror("Autosave failed");
		f->autosave_last = now.tv_sec;
		return;
	}
	f->autosave_chg = f->history.last;
}

void
file_autosave_del(struct File *f)
{
	struct timespec now;
	char name[PATHLEN];
	size_t n;
	f->autosave_chg = f->history.last;
	n = snprintf(name, sizeof(name), "%s~", f->name);
	if (n >= sizeof(name))
		return;
	unlink(name);
	
	if (clock_gettime(CLOCK_MONOTONIC, &now)
	&& clock_gettime(CLOCK_REALTIME, &now))
		return;
	f->autosave_last = now.tv_sec;
}

void
file_change(struct File *f, int64 p0, int64 p1, const char *str, int64 len, int src)
{
	struct Change *c = malloc(sizeof(*c));
	char *from = strdup(text_ref(f->txt, p0, p1));
	if (!from || !c)
		die("out of memory\n");
	memset(c, 0, sizeof(*c));
	
	c->source = src;
	c->from = from;
	c->to = strndup(str, len);
	c->p0 = p0;
	c->p1 = p1;
	c->len = len;
	file_add_change(f, c);
}

void
file_free(struct File *f)
{
	struct Change *c, *next;
	struct Proc *p;
	for (p = procs; p; p = p->next)
		proc_detach_file(p, f);
	(void) text_free(f->txt);
	c = f->history.first;
	while (c) {
		next = c->later;
		free(c);
		c = next;
	}
	node_freelist(f->doc);
	free(f);
}

struct File*
file_new(void)
{
	struct File *f = malloc(sizeof(*f));
	struct Change *c = malloc(sizeof(*c));
	if (!f || !c)
		die("out of memory\n");
	memset(f, 0, sizeof(*f));
	memset(c, 0, sizeof(*c));
	f->txt = text_new();
	f->doc = node_new_root();
	c->final = 1;
	change_add(&f->history, c);
	return f;
}

static int
_plain_write(int fd, Text *tx, struct DocNode *root)
{
	char buf[DOC_TXT_MAX];
	struct DocNode *n;
	int len;
	for (n = root; n->type != DOC_EOF; n = parse_next(tx, NULL, n)) {
		len = node_plain_text(buf, sizeof(buf), tx, n);
		while (len > 0) {
			size_t wlen = write(fd, buf, len);
			if (wlen <= 0)
				return 1;
			len -= wlen;
		}
	}
	return 0;
}

int
file_put(struct File *f, const char *name)
{
	struct stat stb;
	struct DocNode *root;
	int fd, err = 0;
	fd = open(name, O_WRONLY|O_TRUNC|O_CREAT, 0660);
	if (fd < 0)
		return 1;
	if (!f->plain) {
		if (text_put_fd(f->txt, fd))
			return 1;
	} else {
		root = node_new_root();
		if (!root) {
			close(fd);
			return 1;
		}
		err = _plain_write(fd, f->txt, root);
		node_freelist(root);
	}
	
	if (strcmp(name, f->name) == 0) {
		if (fstat(fd, &stb))
			err = 1;
		else
			f->mtime = stb.st_mtim;
	}
	close(fd);
	return err;
}

void
file_ref(struct FileRef *ref, struct File *file, int64 p0, int64 p1)
{
	memset(ref, 0, sizeof(*ref));
	ref->file = file;
	ref->p0 = p0;
	ref->p1 = p1;
	if (!file->history.last)
		file_change(file, 0, 0, "", 0, SOURCE_CMD);
	ref->ch = file->history.last;
	ref->ch->final = 1;
}

void
file_ref_detach(struct FileRef *r)
{
	r->file = NULL;
	r->ch = NULL;
	r->discard = 1;
}

int
file_ref_get(struct FileRef *ref, int64 *p0, int64 *p1)
{
	if (file_ref_update(ref)) {
		*p0 = ref->p0;
		*p1 = ref->p1;
		return 1;
	}
	*p0 = -1;
	*p1 = -1;
	return 0;
}

void
file_ref_insert(struct FileRef *ref, const char *buf, int64 n)
{
	if (ref->discard)
		return;
	if (file_ref_is_append(ref)) {
		int64 len = text_len(ref->file->txt);
		file_change(ref->file, len, len, buf, n, SOURCE_CMD);
		ref->ch = ref->file->history.last;
		return;
	}
	
	if (!file_ref_update(ref)) {
		ref->ch = NULL;
		ref->file = NULL;
		ref->discard = 1;
		return;
	}
	file_change(ref->file, ref->p0, ref->p1, buf, n, SOURCE_CMD);
	ref->p0 += n;
	ref->p1 = ref->p0;
	ref->ch = ref->file->history.last;
}

int
file_ref_is_append(struct FileRef *r)
{
	return r->p0 < 0;
}

int
file_ref_update(struct FileRef *ref)
{
	struct Change *current, *c;
	int64 p0, p1;
	if (!ref->file)
		return 0;
	if (ref->discard || file_ref_is_append(ref))
		return 1;
	current = ref->file->history.last;
	if (ref->ch == current)
		return 1;
	/* Fail if we're on a different branch of history or the future. */
	for (c = ref->ch; c != current; c = c->next)
		if (!c)
			return 0;
	p0 = ref->p0;
	p1 = ref->p1;
	for (c = ref->ch->next; c; c = c->next) {
		/* Fail if referenced text already got changed by someone else. */
		if (c->p0 <= p1 && c->p1 >= p0)
			return 0;
		if (p0 > c->p0) {
			int64 delta = c->len - (c->p1 - c->p0);
			p0 += delta;
			p1 += delta;
		}
		if (c == current)
			break;
	}
	ref->ch = current;
	ref->p0 = p0;
	ref->p1 = p1;
	current->final = 1;
	return 1;
}
