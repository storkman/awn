enum { SOURCE_OTHER, SOURCE_TYPED, SOURCE_CMD };
struct Change {
	uint8 source;
	uint8 final;
	char *from, *to;
	int64 p0, p1, len;
	struct Change *prev, *next;
	struct Change *earlier, *later;
};

typedef struct Proc Proc;

struct File {
	Text	*txt;
	struct DocNode	*doc;
	
	char	name[PATHLEN];
	uint8	virtual, readonly, plain;
	
	struct History {
		struct Change *first, *last, *latest;
	} history;
	
	Proc *client;
	
	struct Change	*save_chg;
	struct timespec	mtime;
	
	time_t	autosave_last;
	struct Change	*autosave_chg;
};

struct FileRef {
	struct File *file;
	int discard;
	struct Change *ch;
	int64 p0, p1;
};

struct Change*	change_add(struct History*, struct Change*);
void	change_free(struct Change*);
int	change_merge(struct Change*, struct Change*);
void	change_rev(struct Change*);

struct Change*	file_add_change(struct File*, struct Change*);
void	file_autosave(struct File*);
void	file_autosave_del(struct File*);
void	file_change(struct File*, int64 p0, int64 p1, const char *str, int64 len, int src);
void	file_free(struct File*);
struct File*	file_new(void);
int	file_put(struct File*, const char*);
void	file_ref(struct FileRef*, struct File*, int64 p0, int64 p1);
void	file_ref_detach(struct FileRef*);
void	file_ref_insert(struct FileRef*, const char*, int64);
int	file_ref_is_append(struct FileRef*);
int	file_ref_update(struct FileRef*);
